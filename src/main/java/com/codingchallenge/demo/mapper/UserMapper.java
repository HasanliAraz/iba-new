package com.codingchallenge.demo.mapper;

import com.codingchallenge.demo.dto.*;
import com.codingchallenge.demo.entities.*;
import org.apache.ibatis.annotations.*;

@Mapper
public interface UserMapper {
    @Results(value = {
            @Result(property = "id", column = "id")
    })
    @Insert("INSERT INTO public.organizations (name, phone, address) " +
            "VALUES (#{signupDTO.name}, #{signupDTO.phone},#{signupDTO.address})")
    void createOrganizationUser(SignupDTO signupDTO);

    @Insert("INSERT INTO public.organizations (name, phone, address) " +
            "VALUES (#{name}, #{phone},#{address})")
    @SelectKey(statement = "SELECT currval('organizations_id_seq')", keyProperty = "id", before = false, resultType = int.class)
    void createOrganization(Organizations org);

    @Insert("INSERT INTO public.staff (organization_id, user_id) " +
            "VALUES (#{organizationId}, #{userId})")
    void createStaff(String userId, String organizationId);

    @Delete("DELETE from users where id=#{oid}")
    void deleteUser(@Param("oid") int oid);


    @Insert("INSERT INTO public.users (name, surname, email, psw, role) " +
            "VALUES (#{name}, #{surname},#{email}, #{psw},  #{role})")
    @SelectKey(statement = "SELECT currval('users_id_seq')", keyProperty = "id", before = false, resultType = int.class)
    void createUser(User user);


    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "surname", column = "surname"),
            @Result(property = "email", column = "email"),
            @Result(property = "psw", column = "psw")
    })
    @Select("select * from users  where name=#{name}")
    UserResponseDTO getUserByName(@Param("name") String name);

    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "createdAt", column = "createdAt"),
            @Result(property = "name", column = "name"),
            @Result(property = "surname", column = "surname"),
            @Result(property = "email", column = "email"),
            @Result(property = "psw", column = "psw"),
            @Result(property = "role", column = "role")
    })
    @Select("select * from users  where name=#{name}")
    User getUserByNameAll(@Param("name") String name);



    @Update("UPDATE public.users SET name=#{userUpdateDTO.name}, surname=#{userUpdateDTO.surname}, email=#{userUpdateDTO.email}, psw=#{userUpdateDTO.password} where id=#{userId}")
    void updateUser(@Param("userId") int userId, UserUpdateDTO userUpdateDTO);

}
