package com.codingchallenge.demo.service;

import com.codingchallenge.demo.dto.AssignTaskDTO;
import com.codingchallenge.demo.dto.TaskDTO;
import com.codingchallenge.demo.dto.TaskResponseDTO;
import com.codingchallenge.demo.dto.TaskUpdateDTO;

import java.util.List;

public interface TaskService {

    TaskResponseDTO getTaskByTaskId(int taskId);

    void addTask(TaskDTO taskDTO);

    void assign(AssignTaskDTO assignTaskDTO);

    void deleteTask(int taskId);

    List<TaskResponseDTO> getTaskByUserId(String userId);

    void unAssign(AssignTaskDTO assignTaskDTO);

    void updateTask(int taskID,TaskUpdateDTO taskUpdateDTO);


}
