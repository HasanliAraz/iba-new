package com.codingchallenge.demo.service;


import com.codingchallenge.demo.dto.SignupDTO;
import com.codingchallenge.demo.dto.UserDTO;
import com.codingchallenge.demo.dto.UserResponseDTO;
import com.codingchallenge.demo.dto.UserUpdateDTO;


public interface UserService {
    void createOrganizationUser(SignupDTO body);
    void createUser(String admin, String password,UserDTO body);
    void deleteUser(int userId);
    UserResponseDTO getUserByName( String name);
    void updateUser(int userId,UserUpdateDTO body);
}
