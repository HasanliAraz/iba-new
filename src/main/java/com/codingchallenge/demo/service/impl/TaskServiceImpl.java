package com.codingchallenge.demo.service.impl;

import com.codingchallenge.demo.dto.AssignTaskDTO;
import com.codingchallenge.demo.dto.TaskDTO;
import com.codingchallenge.demo.dto.TaskResponseDTO;
import com.codingchallenge.demo.dto.TaskUpdateDTO;
import com.codingchallenge.demo.entities.Task;
import com.codingchallenge.demo.enums.StatusEnum;
import com.codingchallenge.demo.mapper.TaskMapper;
import com.codingchallenge.demo.objectmapper.TaskObjectMapper;
import com.codingchallenge.demo.service.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskMapper taskMapper;

    @Override
    public TaskResponseDTO getTaskByTaskId(int taskId) {
        // todo: add task mapper
//        TaskResponseDTO hello_world = TaskResponseDTO.builder()
//                .taskId("1")
//                .status(StatusEnum.DONE)
//                .title("hello world")
//                .build();
//
//        try {
        TaskResponseDTO taskResponseDTO = taskMapper.getTaskByTaskId(taskId);
//            hello_world = taskByTaskId;
//        } catch (Exception e) {
//            log.error("find task error", e);
//        }

        return taskResponseDTO;
    }

    @Override
    public void addTask(TaskDTO taskDTO) {
        Task task = TaskObjectMapper.INSTANCE.getTask(taskDTO);
        taskMapper.addTask2(task);
        log.info(String.valueOf(task.getId()));
    }

    @Override
    public void assign(AssignTaskDTO assignTaskDTO) {
        taskMapper.assign(assignTaskDTO);
    }

    @Override
    public void deleteTask(int taskId) {
        taskMapper.deleteTask(taskId);
    }

    @Override
    public List<TaskResponseDTO> getTaskByUserId(String userId) {
        List<TaskResponseDTO> taskByUserIds = taskMapper.getTaskByUserId(userId);
        return taskByUserIds;
    }

    @Override
    public void unAssign(AssignTaskDTO assignTaskDTO) {
        taskMapper.unAssign(assignTaskDTO);
    }

    @Override
    public void updateTask(int taskId, TaskUpdateDTO taskUpdateDTO) {
        taskMapper.updateTask(taskId, taskUpdateDTO);
    }
}
