package com.codingchallenge.demo.objectmapper;


import com.codingchallenge.demo.dto.TaskDTO;
import com.codingchallenge.demo.entities.Task;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface TaskObjectMapper {

    TaskObjectMapper INSTANCE = Mappers.getMapper(TaskObjectMapper.class);

    @Mappings({
            @Mapping(target = "id",ignore = true),
            @Mapping(target = "createdAt",ignore = true),
            @Mapping(source = "dto.title", target = "title"),
            @Mapping(source = "dto.description", target = "description"),
            @Mapping(source = "dto.deadline", target = "deadline"),
//            @Mapping(source = "dto.status", target = "status"),
            @Mapping(target = "status", ignore = true),
            @Mapping(source = "dto.userId", target = "userId")
    })
    Task getTask(TaskDTO dto);

}
