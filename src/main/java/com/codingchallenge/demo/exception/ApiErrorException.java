package com.codingchallenge.demo.exception;

import com.codingchallenge.demo.dto.ErrorDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiErrorException extends RuntimeException {
    private ErrorDTO appError;
}
