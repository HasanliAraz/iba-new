package com.codingchallenge.demo.exception;

import com.codingchallenge.demo.dto.ErrorMessagesDTO;
import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class ApiValidationException extends ApiException {
    public ApiValidationException(List<ErrorMessagesDTO> message) {
        super(message);
    }
}


