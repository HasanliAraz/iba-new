package com.codingchallenge.demo.enums;

public enum Status {
    USER_ALREADY_EXISTS,
    SUCCESS,
    FAILURE
}
